package com.elt.acmeroster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.widget.Toast;

/**
 * Created by mikelpascual on 09/09/2015.
 */
public class Receivers {
    private static final String ELT_ACTION_RESULT = "com.elt.roster.RESULT";
    private static final String ELT_SCHEME = "pass";
    private static final String ELT_AUTHORITY = "www.passsystem.com";

    private static final String KEY_VISIT_ID = "visitID";
    private static final String KEY_STATUS_CODE = "statusCode";
    private static final String KEY_STATUS_DESCRIPTION = "statusDescription";
    private static final String KEY_ERROR_MSG = "errorMessage";
    private static final String KEY_VISIT_FINISHED = "visitFinished";
    private static final String KEY_VISIT_OUTCOME = "visitOutcome";
    private static final String KEY_VISIT_NOTE = "visitNote";

    private static final String KEY_CALLBACK_URL = "callbackURL";

    private static BroadcastReceiver mReceiver;

    public static void registerReceiver(Context context) {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ELT_ACTION_RESULT);
        filter.addDataScheme(ELT_SCHEME);

        if (mReceiver==null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Uri result = intent.getData();

                    if (result.getQueryParameter(KEY_STATUS_CODE).equals("201")) {
                        Toast.makeText(context, String.format("Completed visit '%s' in PASSsystem, status '%s'", result.getQueryParameter(KEY_VISIT_ID), result.getQueryParameter(KEY_VISIT_OUTCOME)), Toast.LENGTH_SHORT).show();
                        Toast.makeText(context, String.format("Visit note: '%s'", result.getQueryParameter(KEY_VISIT_NOTE)), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, String.format("ERROR '%s', description: '%s'", result.getQueryParameter(KEY_STATUS_CODE), result.getQueryParameter(KEY_STATUS_DESCRIPTION)), Toast.LENGTH_SHORT).show();
                        Toast.makeText(context, result.getQueryParameter(KEY_ERROR_MSG), Toast.LENGTH_SHORT).show();
                    }
                    if (result.getQueryParameter(KEY_CALLBACK_URL)!=null){
                        Toast.makeText(context, "Sorry, URL callbacks are only executed when sent from a webpage", Toast.LENGTH_SHORT).show();
                    }
                    context.getApplicationContext().unregisterReceiver(this);
                    mReceiver = null;
                    startNextActivity(context);
                }
            };

            context.getApplicationContext().registerReceiver(mReceiver, filter);
        }
    }

    private static void startNextActivity(Context context){
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}
