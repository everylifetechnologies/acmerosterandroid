package com.elt.acmeroster;

import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String ELT_ACTION_LAUNCH = "com.elt.roster.action.LAUNCH";
    private static final String ELT_SCHEME = "pass";
    private static final String ELT_AUTHORITY = "www.passgenius.com";

    private static final String KEY_CAREWORKER_ID = "careworkerID";
    private static final String KEY_CAREWORKER_ROSTER_ID = "careworkerRosterID";
    private static final String KEY_CAREWORKER_USERNAME = "careworkerUsername";
    private static final String KEY_CAREWORKER_FULLNAME = "careworkerFullName";
    private static final String KEY_CUSTOMER_ID = "customerID";
    private static final String KEY_CUSTOMER_ROSTER_ID = "customerRosterID";
    private static final String KEY_CUSTOMER_FULLNAME = "customerFullName";
    private static final String KEY_BOOKING_ID = "bookingID";
    private static final String KEY_VISIT_ID = "visitID";
    private static final String KEY_CUSTOMER_FIRST_NAME = "customerFirstName";
    private static final String KEY_CUSTOMER_SURNAME = "customerSurname";
    private static final String KEY_CUSTOMER_POSTCODE = "customerPostcode";
    private static final String KEY_CALLBACK_URL = "callbackURL";
    private static final String KEY_CALLBACK_ACTION = "callbackAction";

    private static final String HARDCODED_CAREWORKER_ID = "5964";
    private static final String HARDCODED_CAREWORKER_ROSTERID = "944001";
    private static final String HARDCODED_CAREWORKER_USERNAME = "bmdev";
    private static final String HARDCODED_CAREWORKER_FULLNAME = "Smith, Mr Jon";
    private static final String HARDCODED_BOOKING_ID = "851748";
    private static final String HARDCODED_VISIT_ID = "17518";
    private static final String HARDCODED_CUSTOMER_ID = "13423";
    private static final String HARDCODED_CUSTOMER_ROSTER_ID = "944002";
    private static final String HARDCODED_CUSTOMER_FULLNAME = "Barrett, Mr Michael";
    private static final String HARDCODED_CUSTOMER_NAME = "Jane";
    private static final String HARDCODED_CUSTOMER_SURNAME = "Doe";
    private static final String HARDCODED_CUSTOMER_POSTCODE = "GU9 7LP";
    private static final String HARDCODED_CALLBACK_URL = "http://roster.example.passgenius.com";

    EditText mCareworkerId;
    EditText mCareworkerRosterId;
    EditText mCareworkerUsername;
    EditText mCareworkerFullName;
    EditText mBookingId;
    EditText mVisitId;
    EditText mCustomerId;
    EditText mCustomerRosterId;
    EditText mCustomerFullName;
    EditText mCustomerFirstName;
    EditText mCustomerSurname;
    EditText mCustomerPostcode;
    EditText mCallbackURL;
    EditText mCallbackAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mCareworkerId = (EditText) findViewById(R.id.careworker_id_ctnt);
        mCareworkerRosterId = (EditText) findViewById(R.id.careworker_rid_ctnt);
        mCareworkerUsername = (EditText) findViewById(R.id.careworker_username_ctnt);
        mCareworkerFullName = (EditText) findViewById(R.id.careworker_fullname_ctnt);
        mBookingId = (EditText) findViewById(R.id.booking_id_ctnt);
        mVisitId = (EditText) findViewById(R.id.visit_id_ctnt);
        mCustomerId = (EditText) findViewById(R.id.customer_id_ctnt);
        mCustomerRosterId = (EditText) findViewById(R.id.customer_rid_ctnt);
        mCustomerFullName = (EditText) findViewById(R.id.customer_fullname_ctnt);
        mCustomerFirstName = (EditText) findViewById(R.id.customer_name_ctnt);
        mCustomerSurname = (EditText) findViewById(R.id.customer_surname_ctnt);
        mCustomerPostcode = (EditText) findViewById(R.id.customer_postcode_ctnt);
        mCallbackURL = (EditText) findViewById(R.id.callback_url_ctnt);
        mCallbackAction = (EditText) findViewById(R.id.callback_action_ctnt);
        Button submitButton = (Button) findViewById(R.id.submit_data);

        // CallbackAction for this app would be com.thirdparty.android.action.LAUNCH

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfo = am.getRunningAppProcesses();
                Uri uri = new Uri.Builder()
                        .scheme(ELT_SCHEME)
                        .authority(ELT_AUTHORITY)
                                //.path("carefree")
                        .encodedQuery(getEncodedQuery())
                        .build();
                Intent i = new Intent(ELT_ACTION_LAUNCH, uri);
                try {
                    startActivity(i);
                    Receivers.registerReceiver(MainActivity.this);
                } catch (ActivityNotFoundException e){
                    Toast.makeText(MainActivity.this, String.format("Please install PASSsystem"), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private String getEncodedQuery() {
        StringBuilder sb = new StringBuilder();
        if (!mCareworkerId.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CAREWORKER_ID, mCareworkerId.getText().toString()));
        }
        if (!mCareworkerUsername.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CAREWORKER_USERNAME, mCareworkerUsername.getText().toString()));
        }
        if (!mCareworkerFullName.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CAREWORKER_FULLNAME, mCareworkerFullName.getText().toString()));
        }
        if (!mBookingId.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_BOOKING_ID, mBookingId.getText().toString()));
        }
        if (!mVisitId.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_VISIT_ID, mVisitId.getText().toString()));
        }
        if (!mCustomerId.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CUSTOMER_ID, mCustomerId.getText().toString()));
        }
        if (!mCustomerFirstName.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CUSTOMER_FIRST_NAME, mCustomerFirstName.getText().toString()));
        }
        if (!mCustomerSurname.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CUSTOMER_SURNAME, mCustomerSurname.getText().toString()));
        }
        if (!mCustomerPostcode.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CUSTOMER_POSTCODE, mCustomerPostcode.getText().toString()));
        }
        if (!mCareworkerRosterId.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CAREWORKER_ROSTER_ID, mCareworkerRosterId.getText().toString()));
        }
        if (!mCustomerRosterId.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CUSTOMER_ROSTER_ID, mCustomerRosterId.getText().toString()));
        }
        if (!mCustomerFullName.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CUSTOMER_FULLNAME, mCustomerFullName.getText().toString()));
        }
        if (!mCallbackURL.getText().toString().isEmpty()){
            sb.append(String.format("%s=%s&", KEY_CALLBACK_URL, mCallbackURL.getText().toString()));
        }

        if (!mCallbackAction.getText().toString().isEmpty()) {
            sb.append(String.format("%s=%s&", KEY_CALLBACK_ACTION, mCallbackAction.getText().toString()));
        }

        if (sb.lastIndexOf("&") != -1) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_prefill_visit_min:
                mCareworkerId.setText(HARDCODED_CAREWORKER_ID);
                mCareworkerRosterId.setText("");
                mCareworkerUsername.setText("");
                mCareworkerFullName.setText("");
                mBookingId.setText("");
                mVisitId.setText(HARDCODED_VISIT_ID);
                mCustomerId.setText(HARDCODED_CUSTOMER_ID);
                mCustomerRosterId.setText("");
                mCustomerFullName.setText("");
                mCustomerFirstName.setText("");
                mCustomerSurname.setText("");
                mCustomerPostcode.setText("");
                mCallbackURL.setText("");
                break;
            case R.id.action_prefill_visit_max:
                mCareworkerId.setText(HARDCODED_CAREWORKER_ID);
                mCareworkerRosterId.setText(HARDCODED_CAREWORKER_ROSTERID);
                mCareworkerUsername.setText(HARDCODED_CAREWORKER_USERNAME);
                mCareworkerFullName.setText(HARDCODED_CAREWORKER_FULLNAME);
                mBookingId.setText(HARDCODED_BOOKING_ID);
                mVisitId.setText(HARDCODED_VISIT_ID);
                mCustomerId.setText(HARDCODED_CUSTOMER_ID);
                mCustomerRosterId.setText(HARDCODED_CUSTOMER_ROSTER_ID);
                mCustomerFullName.setText(HARDCODED_CUSTOMER_FULLNAME);
                mCustomerFirstName.setText(HARDCODED_CUSTOMER_NAME);
                mCustomerSurname.setText(HARDCODED_CUSTOMER_SURNAME);
                mCustomerPostcode.setText(HARDCODED_CUSTOMER_POSTCODE);
                mCallbackURL.setText(HARDCODED_CALLBACK_URL);
                break;
            case R.id.action_prefill_booking_min:
                mCareworkerId.setText(HARDCODED_CAREWORKER_ID);
                mCareworkerRosterId.setText("");
                mCareworkerUsername.setText("");
                mCareworkerFullName.setText("");
                mBookingId.setText(HARDCODED_BOOKING_ID);
                mVisitId.setText("");
                mCustomerId.setText(HARDCODED_CUSTOMER_ID);
                mCustomerRosterId.setText("");
                mCustomerFullName.setText("");
                mCustomerFirstName.setText("");
                mCustomerSurname.setText("");
                mCustomerPostcode.setText("");
                mCallbackURL.setText("");
                break;
            case R.id.action_prefill_booking_max:
                mCareworkerId.setText(HARDCODED_CAREWORKER_ID);
                mCareworkerRosterId.setText(HARDCODED_CAREWORKER_ROSTERID);
                mCareworkerUsername.setText(HARDCODED_CAREWORKER_USERNAME);
                mCareworkerFullName.setText(HARDCODED_CAREWORKER_FULLNAME);
                mBookingId.setText(HARDCODED_BOOKING_ID);
                mVisitId.setText("");
                mCustomerId.setText(HARDCODED_CUSTOMER_ID);
                mCustomerRosterId.setText("");
                mCustomerFullName.setText(HARDCODED_CUSTOMER_FULLNAME);
                mCustomerFirstName.setText(HARDCODED_CUSTOMER_NAME);
                mCustomerSurname.setText(HARDCODED_CUSTOMER_SURNAME);
                mCustomerPostcode.setText(HARDCODED_CUSTOMER_POSTCODE);
                mCallbackURL.setText(HARDCODED_CALLBACK_URL);
                break;
            case R.id.action_prefill_customer_min:
                mCareworkerId.setText(HARDCODED_CAREWORKER_ID);
                mCareworkerRosterId.setText("");
                mCareworkerUsername.setText("");
                mCareworkerFullName.setText("");
                mBookingId.setText("");
                mVisitId.setText("");
                mCustomerId.setText(HARDCODED_CUSTOMER_ID);
                mCustomerRosterId.setText("");
                mCustomerFullName.setText("");
                mCustomerFirstName.setText("");
                mCustomerSurname.setText("");
                mCustomerPostcode.setText("");
                mCallbackURL.setText("");
                break;
            case R.id.action_prefill_customer_max:
                mCareworkerId.setText(HARDCODED_CAREWORKER_ID);
                mCareworkerRosterId.setText(HARDCODED_CAREWORKER_ROSTERID);
                mCareworkerUsername.setText(HARDCODED_CUSTOMER_SURNAME);
                mCareworkerFullName.setText(HARDCODED_CAREWORKER_FULLNAME);
                mBookingId.setText("");
                mVisitId.setText("");
                mCustomerId.setText(HARDCODED_CUSTOMER_ID);
                mCustomerRosterId.setText("");
                mCustomerFullName.setText(HARDCODED_CUSTOMER_FULLNAME);
                mCustomerFirstName.setText(HARDCODED_CUSTOMER_NAME);
                mCustomerSurname.setText(HARDCODED_CUSTOMER_SURNAME);
                mCustomerPostcode.setText(HARDCODED_CUSTOMER_POSTCODE);
                mCallbackURL.setText(HARDCODED_CALLBACK_URL);
                break;
            case R.id.action_prefill_roster_min:
                mCareworkerId.setText("");
                mCareworkerRosterId.setText(HARDCODED_CAREWORKER_ROSTERID);
                mCareworkerUsername.setText("");
                mCareworkerFullName.setText("");
                mBookingId.setText("");
                mVisitId.setText("");
                mCustomerId.setText("");
                mCustomerRosterId.setText(HARDCODED_CUSTOMER_ROSTER_ID);
                mCustomerFullName.setText("");
                mCustomerFirstName.setText("");
                mCustomerSurname.setText("");
                mCustomerPostcode.setText("");
                mCallbackURL.setText("");
                break;
            case R.id.action_prefill_roster_max:
                mCareworkerId.setText(HARDCODED_CAREWORKER_ID);
                mCareworkerRosterId.setText(HARDCODED_CAREWORKER_ROSTERID);
                mCareworkerUsername.setText(HARDCODED_CAREWORKER_USERNAME);
                mCareworkerFullName.setText(HARDCODED_CAREWORKER_FULLNAME);
                mBookingId.setText("");
                mVisitId.setText("");
                mCustomerId.setText(HARDCODED_CUSTOMER_ID);
                mCustomerRosterId.setText(HARDCODED_CUSTOMER_ROSTER_ID);
                mCustomerFullName.setText(HARDCODED_CUSTOMER_FULLNAME);
                mCustomerFirstName.setText(HARDCODED_CUSTOMER_NAME);
                mCustomerSurname.setText(HARDCODED_CUSTOMER_SURNAME);
                mCustomerPostcode.setText(HARDCODED_CUSTOMER_POSTCODE);
                mCallbackURL.setText(HARDCODED_CALLBACK_URL);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    void openPASSsystemAndListenForBroadCast() {
        final String ELT_ACTION_LAUNCH = "com.elt.roster.action.LAUNCH";
        final String ELT_SCHEME = "pass";
        final String ELT_AUTHORITY = "www.passgenius.com";

        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        Uri uri = new Uri.Builder()
                .scheme(ELT_SCHEME)
                .authority(ELT_AUTHORITY)
                .encodedQuery(getEncodedQuery())
                .build();
        Intent i = new Intent(ELT_ACTION_LAUNCH, uri);
        try {
            startActivity(i);
            Receivers.registerReceiver(MainActivity.this);
        } catch (ActivityNotFoundException e){
            Toast.makeText(MainActivity.this, String.format("Please install PASSsystem"), Toast.LENGTH_SHORT).show();
        }
    }

}
