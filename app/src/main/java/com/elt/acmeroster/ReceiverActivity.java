package com.elt.acmeroster;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Patterns;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by bahman on 07/04/2016.
 */

public class ReceiverActivity extends Activity {
    private static final String ELT_ACTION_LAUNCH = "com.elt.roster.LAUNCH";
    private static final String ANDROID_ACTION_VIEW = "android.intent.action.VIEW";
    private static final String ELT_ACTION_RESULT = "com.elt.roster.RESULT";
    private static final String ANDROID_CATEGORY_BROWSABLE = "android.intent.category.BROWSABLE";
    private static final String ELT_SCHEME = "pass";
    private static final String ELT_AUTHORITY = "www.passgenius.com";

    private static Scope mCallerScope = Scope.UNSET;
    private static Activity mActivity;
    private static Intent mIntent;
    private static Uri mUri;

    private static AtomicBoolean mAttendingBroadcast = new AtomicBoolean(false);

    private static final String KEY_CAREWORKER_ID = "careworkerID";
    private static final String KEY_CAREWORKER_ROSTER_ID = "careworkerRosterID";
    private static final String KEY_CUSTOMER_ID = "customerID";
    private static final String KEY_CUSTOMER_ROSTER_ID = "customerRosterID";
    private static final String KEY_BOOKING_ID = "bookingID";
    private static final String KEY_ROLE_ID = "role";
    private static final String KEY_VISIT_ID = "visitID";
    private static final String KEY_CAREWORKER_USERNAME = "careworkerUsername";
    private static final String KEY_CAREWORKER_FULLNAME = "careworkerFullName";
    private static final String KEY_CUSTOMER_NAME = "customerFirstName";
    private static final String KEY_CUSTOMER_SURNAME = "customerSurname";
    private static final String KEY_CUSTOMER_POSTCODE = "customerPostcode";
    private static final String KEY_CUSTOMER_FULLNAME = "customerFullName";
    private static final String KEY_CALLBACK_URL = "callbackURL";
    private static final String KEY_CALLBACK_ACTION = "callbackAction";

    private static final String VALUE_ROLE_LEAD = "lead";
    private static final String VALUE_ROLE_NONLEAD = "nonlead";

    private static final String KEY_STATUS_CODE = "statusCode";
    private static final String KEY_STATUS_DESCRIPTION = "statusDescription";
    private static final String KEY_ERROR_MSG = "errorMessage";
    private static final String KEY_VISIT_FINISHED = "visitFinished";
    private static final String KEY_VISIT_OUTCOME = "visitOutcome";
    private static final String KEY_VISIT_NOTE = "visitNote";

    public static final int REQ_CODE_NEW_CREDENTIALS = 1111;
    public static final int RESULT_ERROR = 1;
    public static final String EXTRA_MESSAGE = "message";
    public static final String EXTRA_REQ_CODE = "requestCode";


    public enum Scope {
        LOGIN,
        CUSTOMER,
        BOOKING,
        VISIT,
        UNSET,
        DIE
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
        if (!mAttendingBroadcast.get()) {
            mAttendingBroadcast.getAndSet(true);
            mActivity = this;
            mIntent = getIntent();

            mUri = getIntent().getData();

            Toast.makeText(this, mUri.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        mAttendingBroadcast.getAndSet(false);
        super.onDestroy();
    }
}

